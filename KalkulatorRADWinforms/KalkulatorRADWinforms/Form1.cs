﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KalkulatorRADWinforms
{
    public partial class Form1 : Form
    {
        string number = "";
        string operation = "";
        int system = 10;
     
        public Form1()
        {
            InitializeComponent();
            result.ReadOnly = true;
            this.FormBorderStyle = FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;

        }
      
        private void button_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            result.Text = result.Text + b.Text;
            result.ForeColor = Color.Red;
        }
        private void buttonClear_Click(object sender, EventArgs e)
        {
            result.Text = "";
            number = "";
            labelNumber.Text = "";
            operation = "";
        }
        private void operator_Click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (result.Text != "" && number != "")
            {
                operation = b.Text;
                buttonFinal_Click(sender, e);
                return;
            }
            else if(number == "" && labelNumber.Text != "")
            {
                number = labelNumber.Text;
            }
            else if (number == "" && result.Text == "" && labelNumber.Text == "")
            {
                return;
            }
            else if (number == "")
            {
                number = result.Text;
            }

            operation = b.Text;
            
            //number = result.Text;
            labelNumber.Text = number + operation;
            result.Clear();
            result.Focus();
            
        }
        private void buttonFinal_Click(object sender, EventArgs e)
        {
            if(number == "" || result.Text == "")
            {
                return;
            }
            switch(operation)
            {
                case "+":
                    string additionResult = ConvertNumericalSystem.Addition(system, number, result.Text);
                    result.Text = "";
                    labelNumber.Text = additionResult.ToString();
                    break;
                case "-":
                    string substractionResult = ConvertNumericalSystem.Substraction(system, number, result.Text);
                    result.Text = "";
                    labelNumber.Text = substractionResult.ToString();
                    break;
                case "*":
                    string multiplicationResult = ConvertNumericalSystem.Multiplication(system, number, result.Text);
                    result.Text = "";
                    labelNumber.Text = multiplicationResult.ToString();
                    break;
                case "/":
                    string divisionResult;
                    if ((result.Text == "0") || (result.Text == ""))
                    {
                        result.Text = "";
                        labelNumber.Text = "";
                        number = "";
                        operation = "";
                        MessageBox.Show("Nie dozwolone działanie");
                    }
                    else
                    {
                        divisionResult = ConvertNumericalSystem.Division(system, number, result.Text);
                        labelNumber.Text = divisionResult.ToString();
                        result.Text = "";
                    }
                    break;
            }
            number = "";
        }
        private void system2_Click(object sender, EventArgs e)
        {
            Button s = (Button)sender;
            int temp = ConvertNumericalSystem.ToDecimal(number, system);
            int temp2 = ConvertNumericalSystem.ToDecimal(result.Text, system);

            if (labelNumber.Text != "" && number == "")
            {
                temp = ConvertNumericalSystem.ToDecimal(labelNumber.Text, system);
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            else if (labelNumber.Text != "")
            {
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            system = Convert.ToInt32(s.Text);
            if (result.Text != "")
            {
                labelNumber.Text = number + operation;
                result.Text = ConvertNumericalSystem.FromDecimal(temp2, system);
            }
            else
                labelNumber.Text = number;
            button7Calc.Enabled = false;
            button8Calc.Enabled = false;
            button9Calc.Enabled = false;
            button4Calc.Enabled = false;
            button5Calc.Enabled = false;
            button6Calc.Enabled = false;
            button3Calc.Enabled = false;
            button2Calc.Enabled = false;
            button1Calc.Enabled = true;
            button16.Enabled = true;
        }
        private void system3_Click(object sender, EventArgs e)
        {
            Button s = (Button)sender;
            int temp = ConvertNumericalSystem.ToDecimal(number, system);
            int temp2 = ConvertNumericalSystem.ToDecimal(result.Text, system);

            if (labelNumber.Text != "" && number == "")
            {
                temp = ConvertNumericalSystem.ToDecimal(labelNumber.Text, system);
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            else if (labelNumber.Text != "")
            {
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            system = Convert.ToInt32(s.Text);
            if (result.Text != "")
            {
                labelNumber.Text = number + operation;
                result.Text = ConvertNumericalSystem.FromDecimal(temp2, system);
            }
            else
                labelNumber.Text = number;
            button7Calc.Enabled = false;
            button8Calc.Enabled = false;
            button9Calc.Enabled = false;
            button4Calc.Enabled = false;
            button5Calc.Enabled = false;
            button6Calc.Enabled = false;
            button3Calc.Enabled = false;
            button2Calc.Enabled = true;
            button1Calc.Enabled = true;
            button16.Enabled = true;
        }
        private void system4_Click(object sender, EventArgs e)
        {
            Button s = (Button)sender;
            int temp = ConvertNumericalSystem.ToDecimal(number, system);
            int temp2 = ConvertNumericalSystem.ToDecimal(result.Text, system);

            if (labelNumber.Text != "" && number == "")
            {
                temp = ConvertNumericalSystem.ToDecimal(labelNumber.Text, system);
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            else if (labelNumber.Text != "")
            {
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            system = Convert.ToInt32(s.Text);
            if (result.Text != "")
            {
                labelNumber.Text = number + operation;
                result.Text = ConvertNumericalSystem.FromDecimal(temp2, system);
            }
            else
                labelNumber.Text = number;
            button7Calc.Enabled = false;
            button8Calc.Enabled = false;
            button9Calc.Enabled = false;
            button5Calc.Enabled = false;
            button6Calc.Enabled = false;
            button4Calc.Enabled = false;
            button3Calc.Enabled = true;
            button2Calc.Enabled = true;
            button1Calc.Enabled = true;
            button16.Enabled = true;
        }
        private void system5_Click(object sender, EventArgs e)
        {
            Button s = (Button)sender;
            int temp = ConvertNumericalSystem.ToDecimal(number, system);
            int temp2 = ConvertNumericalSystem.ToDecimal(result.Text, system);

            if (labelNumber.Text != "" && number == "")
            {
                temp = ConvertNumericalSystem.ToDecimal(labelNumber.Text, system);
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            else if (labelNumber.Text != "")
            {
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            system = Convert.ToInt32(s.Text);
            if (result.Text != "")
            {
                labelNumber.Text = number + operation;
                result.Text = ConvertNumericalSystem.FromDecimal(temp2, system);
            }
            else
                labelNumber.Text = number;
            button7Calc.Enabled = false;
            button8Calc.Enabled = false;
            button9Calc.Enabled = false;
            button5Calc.Enabled = false;
            button6Calc.Enabled = false;
            button4Calc.Enabled = true;
            button3Calc.Enabled = true;
            button2Calc.Enabled = true;
            button1Calc.Enabled = true;
            button16.Enabled = true;
        }
        private void system6_Click(object sender, EventArgs e)
        {
            Button s = (Button)sender;
            int temp = ConvertNumericalSystem.ToDecimal(number, system);
            int temp2 = ConvertNumericalSystem.ToDecimal(result.Text, system);

            if (labelNumber.Text != "" && number == "")
            {
                temp = ConvertNumericalSystem.ToDecimal(labelNumber.Text, system);
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            else if (labelNumber.Text != "")
            {
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            system = Convert.ToInt32(s.Text);
            if (result.Text != "")
            {
                labelNumber.Text = number + operation;
                result.Text = ConvertNumericalSystem.FromDecimal(temp2, system);
            }
            else
                labelNumber.Text = number;
            button7Calc.Enabled = false;
            button8Calc.Enabled = false;
            button9Calc.Enabled = false;
            button6Calc.Enabled = false;
            button5Calc.Enabled = true;
            button4Calc.Enabled = true;
            button3Calc.Enabled = true;
            button2Calc.Enabled = true;
            button1Calc.Enabled = true;
            button16.Enabled = true;
        }
        private void system7_Click(object sender, EventArgs e)
        {
            Button s = (Button)sender;
            int temp = ConvertNumericalSystem.ToDecimal(number, system);
            int temp2 = ConvertNumericalSystem.ToDecimal(result.Text, system);

            if (labelNumber.Text != "" && number == "")
            {
                temp = ConvertNumericalSystem.ToDecimal(labelNumber.Text, system);
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            else if (labelNumber.Text != "")
            {
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            system = Convert.ToInt32(s.Text);
            if (result.Text != "")
            {
                labelNumber.Text = number + operation;
                result.Text = ConvertNumericalSystem.FromDecimal(temp2, system);
            }
            else
                labelNumber.Text = number;
            button7Calc.Enabled = false;
            button8Calc.Enabled = false;
            button9Calc.Enabled = false;
            button6Calc.Enabled = true;
            button5Calc.Enabled = true;
            button4Calc.Enabled = true;
            button3Calc.Enabled = true;
            button2Calc.Enabled = true;
            button1Calc.Enabled = true;
            button16.Enabled = true;
        }
        private void system8_Click(object sender, EventArgs e)
        {
            Button s = (Button)sender;
            int temp = ConvertNumericalSystem.ToDecimal(number, system);
            int temp2 = ConvertNumericalSystem.ToDecimal(result.Text, system);

            if (labelNumber.Text != "" && number == "")
            {
                temp = ConvertNumericalSystem.ToDecimal(labelNumber.Text, system);
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            else if (labelNumber.Text != "")
            {
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            system = Convert.ToInt32(s.Text);
            if (result.Text != "")
            {
                labelNumber.Text = number + operation;
                result.Text = ConvertNumericalSystem.FromDecimal(temp2, system);
            }
            else
                labelNumber.Text = number;
            button8Calc.Enabled = false;
            button9Calc.Enabled = false;
            button7Calc.Enabled = true;
            button6Calc.Enabled = true;
            button5Calc.Enabled = true;
            button4Calc.Enabled = true;
            button3Calc.Enabled = true;
            button2Calc.Enabled = true;
            button1Calc.Enabled = true;
            button16.Enabled = true;
        }
        private void system9_Click(object sender, EventArgs e)
        {
            Button s = (Button)sender;
            int temp = ConvertNumericalSystem.ToDecimal(number, system);
            int temp2 = ConvertNumericalSystem.ToDecimal(result.Text, system);

            if (labelNumber.Text != "" && number == "")
            {
                temp = ConvertNumericalSystem.ToDecimal(labelNumber.Text, system);
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            else if (labelNumber.Text != "")
            {
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            system = Convert.ToInt32(s.Text);
            if (result.Text != "")
            {
                labelNumber.Text = number + operation;
                result.Text = ConvertNumericalSystem.FromDecimal(temp2, system);
            }
            else
                labelNumber.Text = number;
            button9Calc.Enabled = false;
            button8Calc.Enabled = true;
            button7Calc.Enabled = true;
            button6Calc.Enabled = true;
            button5Calc.Enabled = true;
            button4Calc.Enabled = true;
            button3Calc.Enabled = true;
            button2Calc.Enabled = true;
            button1Calc.Enabled = true;
            button16.Enabled = true;
        }
        private void system10_Click(object sender, EventArgs e)
        {
            Button s = (Button)sender;
            int temp = ConvertNumericalSystem.ToDecimal(number, system);
            int temp2 = ConvertNumericalSystem.ToDecimal(result.Text, system);
            
            if (labelNumber.Text != "" && number == "")
            {
                temp = ConvertNumericalSystem.ToDecimal(labelNumber.Text, system);
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            else if (labelNumber.Text != "")
            {
                system = Convert.ToInt32(s.Text);
                number = ConvertNumericalSystem.FromDecimal(temp, system);

            }
            system = Convert.ToInt32(s.Text);
            if (result.Text != "")
            {
                labelNumber.Text = number + operation;
                result.Text = ConvertNumericalSystem.FromDecimal(temp2, system);
            }
            else
                labelNumber.Text = number;
            button9Calc.Enabled = true;
            button8Calc.Enabled = true;
            button7Calc.Enabled = true;
            button6Calc.Enabled = true;
            button5Calc.Enabled = true;
            button4Calc.Enabled = true;
            button3Calc.Enabled = true;
            button2Calc.Enabled = true;
            button1Calc.Enabled = true;
            button16.Enabled = true;
        }

        private void buttonBackSpace_Click(object sender, EventArgs e)
        {
            int length = result.TextLength - 1;
            string text = result.Text;
            result.Clear();
            for(int i=0; i <length; i++)
            {
                result.Text = result.Text + text[i];
            }
        }
    }
}
