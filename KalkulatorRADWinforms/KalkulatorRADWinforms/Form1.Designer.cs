﻿namespace KalkulatorRADWinforms
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.result = new System.Windows.Forms.TextBox();
            this.button7Calc = new System.Windows.Forms.Button();
            this.button8Calc = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button9Calc = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6Calc = new System.Windows.Forms.Button();
            this.button5Calc = new System.Windows.Forms.Button();
            this.button4Calc = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button3Calc = new System.Windows.Forms.Button();
            this.button2Calc = new System.Windows.Forms.Button();
            this.button1Calc = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.buttonFinal = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonBackSpace = new System.Windows.Forms.Button();
            this.labelNumber = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // result
            // 
            this.result.Location = new System.Drawing.Point(114, 44);
            this.result.Name = "result";
            this.result.Size = new System.Drawing.Size(197, 20);
            this.result.TabIndex = 0;
            this.result.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button7Calc
            // 
            this.button7Calc.Location = new System.Drawing.Point(156, 116);
            this.button7Calc.Name = "button7Calc";
            this.button7Calc.Size = new System.Drawing.Size(34, 30);
            this.button7Calc.TabIndex = 1;
            this.button7Calc.Text = "7";
            this.button7Calc.UseVisualStyleBackColor = true;
            this.button7Calc.Click += new System.EventHandler(this.button_Click);
            // 
            // button8Calc
            // 
            this.button8Calc.Location = new System.Drawing.Point(196, 116);
            this.button8Calc.Name = "button8Calc";
            this.button8Calc.Size = new System.Drawing.Size(35, 29);
            this.button8Calc.TabIndex = 2;
            this.button8Calc.Text = "8";
            this.button8Calc.UseVisualStyleBackColor = true;
            this.button8Calc.Click += new System.EventHandler(this.button_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(276, 116);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(35, 29);
            this.button3.TabIndex = 4;
            this.button3.Text = "/";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.operator_Click);
            // 
            // button9Calc
            // 
            this.button9Calc.Location = new System.Drawing.Point(236, 116);
            this.button9Calc.Name = "button9Calc";
            this.button9Calc.Size = new System.Drawing.Size(34, 30);
            this.button9Calc.TabIndex = 3;
            this.button9Calc.Text = "9";
            this.button9Calc.UseVisualStyleBackColor = true;
            this.button9Calc.Click += new System.EventHandler(this.button_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(276, 152);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(35, 29);
            this.button5.TabIndex = 8;
            this.button5.Text = "*";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.operator_Click);
            // 
            // button6Calc
            // 
            this.button6Calc.Location = new System.Drawing.Point(236, 152);
            this.button6Calc.Name = "button6Calc";
            this.button6Calc.Size = new System.Drawing.Size(34, 30);
            this.button6Calc.TabIndex = 7;
            this.button6Calc.Text = "6";
            this.button6Calc.UseVisualStyleBackColor = true;
            this.button6Calc.Click += new System.EventHandler(this.button_Click);
            // 
            // button5Calc
            // 
            this.button5Calc.Location = new System.Drawing.Point(196, 152);
            this.button5Calc.Name = "button5Calc";
            this.button5Calc.Size = new System.Drawing.Size(35, 29);
            this.button5Calc.TabIndex = 6;
            this.button5Calc.Text = "5";
            this.button5Calc.UseVisualStyleBackColor = true;
            this.button5Calc.Click += new System.EventHandler(this.button_Click);
            // 
            // button4Calc
            // 
            this.button4Calc.Location = new System.Drawing.Point(156, 152);
            this.button4Calc.Name = "button4Calc";
            this.button4Calc.Size = new System.Drawing.Size(34, 30);
            this.button4Calc.TabIndex = 5;
            this.button4Calc.Text = "4";
            this.button4Calc.UseVisualStyleBackColor = true;
            this.button4Calc.Click += new System.EventHandler(this.button_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(276, 188);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(35, 29);
            this.button9.TabIndex = 12;
            this.button9.Text = "-";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.operator_Click);
            // 
            // button3Calc
            // 
            this.button3Calc.Location = new System.Drawing.Point(236, 188);
            this.button3Calc.Name = "button3Calc";
            this.button3Calc.Size = new System.Drawing.Size(34, 30);
            this.button3Calc.TabIndex = 11;
            this.button3Calc.Text = "3";
            this.button3Calc.UseVisualStyleBackColor = true;
            this.button3Calc.Click += new System.EventHandler(this.button_Click);
            // 
            // button2Calc
            // 
            this.button2Calc.Location = new System.Drawing.Point(196, 188);
            this.button2Calc.Name = "button2Calc";
            this.button2Calc.Size = new System.Drawing.Size(35, 29);
            this.button2Calc.TabIndex = 10;
            this.button2Calc.Text = "2";
            this.button2Calc.UseVisualStyleBackColor = true;
            this.button2Calc.Click += new System.EventHandler(this.button_Click);
            // 
            // button1Calc
            // 
            this.button1Calc.Location = new System.Drawing.Point(156, 188);
            this.button1Calc.Name = "button1Calc";
            this.button1Calc.Size = new System.Drawing.Size(34, 30);
            this.button1Calc.TabIndex = 9;
            this.button1Calc.Text = "1";
            this.button1Calc.UseVisualStyleBackColor = true;
            this.button1Calc.Click += new System.EventHandler(this.button_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(278, 223);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(33, 31);
            this.button13.TabIndex = 16;
            this.button13.Text = "+";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.operator_Click);
            // 
            // buttonFinal
            // 
            this.buttonFinal.Location = new System.Drawing.Point(196, 223);
            this.buttonFinal.Name = "buttonFinal";
            this.buttonFinal.Size = new System.Drawing.Size(76, 30);
            this.buttonFinal.TabIndex = 15;
            this.buttonFinal.Text = "=";
            this.buttonFinal.UseVisualStyleBackColor = true;
            this.buttonFinal.Click += new System.EventHandler(this.buttonFinal_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(156, 223);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(34, 30);
            this.button16.TabIndex = 13;
            this.button16.Text = "0";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(81, 223);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(34, 29);
            this.button15.TabIndex = 20;
            this.button15.Text = "2";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.system2_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(81, 188);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(34, 29);
            this.button17.TabIndex = 19;
            this.button17.Text = "3";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.system3_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(81, 152);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(34, 29);
            this.button18.TabIndex = 18;
            this.button18.Text = "4";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.system4_Click);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(81, 116);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(34, 29);
            this.button19.TabIndex = 17;
            this.button19.Text = "5";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.system5_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(41, 222);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(34, 30);
            this.button20.TabIndex = 24;
            this.button20.Text = "6";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.system6_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(41, 187);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(34, 30);
            this.button21.TabIndex = 23;
            this.button21.Text = "7";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.system7_Click);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(41, 151);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(34, 30);
            this.button22.TabIndex = 22;
            this.button22.Text = "8";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.system8_Click);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(41, 116);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(34, 29);
            this.button23.TabIndex = 21;
            this.button23.Text = "9";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.system9_Click);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(41, 88);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(74, 23);
            this.button24.TabIndex = 28;
            this.button24.Text = "10";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.system10_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(156, 88);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(75, 23);
            this.buttonClear.TabIndex = 29;
            this.buttonClear.Text = "Clear";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonBackSpace
            // 
            this.buttonBackSpace.Location = new System.Drawing.Point(236, 88);
            this.buttonBackSpace.Name = "buttonBackSpace";
            this.buttonBackSpace.Size = new System.Drawing.Size(75, 23);
            this.buttonBackSpace.TabIndex = 30;
            this.buttonBackSpace.Text = "<=";
            this.buttonBackSpace.UseVisualStyleBackColor = true;
            this.buttonBackSpace.Click += new System.EventHandler(this.buttonBackSpace_Click);
            // 
            // labelNumber
            // 
            this.labelNumber.AutoSize = true;
            this.labelNumber.Location = new System.Drawing.Point(115, 47);
            this.labelNumber.Name = "labelNumber";
            this.labelNumber.Size = new System.Drawing.Size(0, 13);
            this.labelNumber.TabIndex = 31;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(38, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 32;
            this.label2.Text = "Systemy";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 285);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.labelNumber);
            this.Controls.Add(this.buttonBackSpace);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.buttonFinal);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button3Calc);
            this.Controls.Add(this.button2Calc);
            this.Controls.Add(this.button1Calc);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button6Calc);
            this.Controls.Add(this.button5Calc);
            this.Controls.Add(this.button4Calc);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button9Calc);
            this.Controls.Add(this.button8Calc);
            this.Controls.Add(this.button7Calc);
            this.Controls.Add(this.result);
            this.Name = "Form1";
            this.Text = "Kalkulator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox result;
        private System.Windows.Forms.Button button7Calc;
        private System.Windows.Forms.Button button8Calc;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button9Calc;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6Calc;
        private System.Windows.Forms.Button button5Calc;
        private System.Windows.Forms.Button button4Calc;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button3Calc;
        private System.Windows.Forms.Button button2Calc;
        private System.Windows.Forms.Button button1Calc;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button buttonFinal;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonBackSpace;
        private System.Windows.Forms.Label labelNumber;
        private System.Windows.Forms.Label label2;
    }
}

