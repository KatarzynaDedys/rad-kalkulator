﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KalkulatorRADWinforms
{
    class ConvertNumericalSystem
    {
        public static string FromDecimal(int value, int system)
        {
            int a = 0;
            if (value < 0)
            {
                a++;
                value = value - (2 * value);
            }
            int resultDivisionInt;
            int resultDivisionModulo;
            List<int> list = new List<int>();
            if (value == 0)
            {
                return "0";
            }
            while (value > 0)
            {
                resultDivisionInt = value / system;
                resultDivisionModulo = value % system;
                value = resultDivisionInt;
                list.Add(resultDivisionModulo);
            }
            list.Reverse();
            string result = string.Join("", list.Select(n => n.ToString()).ToArray());
            if (a > 0)
            {
                result = "-" + result;
            }
            return result;
        }
        public static int ToDecimal(string value, int system)
        {
            int sum = 0;
            int a = 0;
            int index = 0;

            List<char> list = new List<char>();
            List<int> listInt = new List<int>();

            foreach (char x in value)
            {
                if (x == '-')
                {
                    a++;
                    index = 1;
                    continue;
                }
                int xToInt = (int)char.GetNumericValue(x);

                listInt.Add(xToInt);
            }
            listInt.Reverse();
            for (int i = 0; i < listInt.Count; i++)
            {
                sum += (Convert.ToInt32((long)Math.Pow(system, i)) * listInt[i]);
            }
            if (a > 0)
            {
                sum = sum - (2 * sum);
            }
            return sum;
        }
        public static string Addition(int system, string number1, string number2)
        {
            int numberInt1 = ToDecimal(number1, system);
            int numberInt2 = ToDecimal(number2, system);
            int sum = numberInt1 + numberInt2;
            string result = FromDecimal(sum, system);
            return result;
        }
        public static string Substraction(int system, string number1, string number2)
        {
            int numberInt1 = ToDecimal(number1, system);
            int numberInt2 = ToDecimal(number2, system);
            int difference = numberInt1 - numberInt2;
            string result = FromDecimal(difference, system);
            return result;
        }
        public static string Multiplication(int system, string number1, string number2)
        {
            int numberInt1 = ToDecimal(number1, system);
            int numberInt2 = ToDecimal(number2, system);
            int product = numberInt1 * numberInt2;
            string result = FromDecimal(product, system);
            return result;
        }
        public static string Division(int system, string number1, string number2)
        {
            int numberInt1 = ToDecimal(number1, system);
            int numberInt2 = ToDecimal(number2, system);
            int quotient = numberInt1 / numberInt2;
            string result = FromDecimal(quotient, system);
            return result;
        }

    }
}
